<?php

Route::get('/', function (){
    return view('welcome');
});

//Assignment 1 Route
Route::get('assignment1/{locale?}', function ($locale=null) {
    if(isset($locale)) {
        App::setLocale($locale);
    }

    //************************************************************//
    //*******************Locale Information***********************//
    //************************************************************//

    //Get Locale From HTTP Header
    $lang = Request::server('HTTP_ACCEPT_LANGUAGE');

    //IP Address
    $ipAddress = Request::ip();

    //Get abbreviated Locale using the HTTP Header and the acceptFromHTTP Method
    $localeHTTP = Locale::acceptFromHTTP( Request::server('HTTP_ACCEPT_LANGUAGE'));

    //Code to figure out whether user is using a twelve hour clock
    //I couldn't find a method that checks whether the clock is 12 or 24 so made this function
    $twelveHourCountryCodes = ['US', 'GB', 'PH', 'CA', 'AU', 'NZ', 'IN', 'EG', 'SA', 'CO', 'PK', 'MY'];

    //Get country code from Header Locale (e.g. en_US would result in $countryCode[0]=en & $countryCode[1]=US
    //Get Primary Accept language from HTTP Header
    $localeHTTP = \Locale::acceptFromHTTP( \Request::server('HTTP_ACCEPT_LANGUAGE'));
    //Get country code from Header Locale (e.g. en_US would result in $countryCode[0]=en & $countryCode[1]=US
    $explodedLocale = explode("_", $localeHTTP);
    //Set supported markets by API (Used for billingRegion and marketid

    //Check to see that there is more than 1 value.
    if(count($explodedLocale) > 1){
        $languageCode = strtolower($explodedLocale[0]);
        $countryCode =  strtolower($explodedLocale[1]);
        //Through testing I found that UK is GB in the Chrome browser
        if($countryCode == 'gb'){
            $countryCode = 'uk';
        }
    }else{
        //Some countries use both the language and country as the same identifier (e.g. Germany with de)
        $languageCode = $localeHTTP;
        $countryCode = $localeHTTP;
        //Create new locale that is formatted correctly for functions/methods
        $localeHTTP = $languageCode . "_" . strtoupper($countryCode);
    }

    if(in_array($countryCode[1], $twelveHourCountryCodes))
    {
        $clockFormat = "12 hour clock";
    }
    else
    {
        $clockFormat = "24 hour clock";
    }

    //Default Locale
    $defaultLocale = Locale::getDefault ( );

    //Display Language
    $displayLanguage = Locale::getDisplayLanguage($localeHTTP);

    //Display Locale Region
    $displayRegion = Locale::getDisplayRegion($localeHTTP);

    //Display Locale Script
    $displayScript = Locale::getDisplayScript( 'sl-Latn-IT-nedis', $languageCode);


    //**************************************************************//
    //********************Calendar/Date/Time************************//
    //**************************************************************//

    //Create and International Calendar Instance Using Locale
    $intCalendar = IntlCalendar::createInstance(NULL, $localeHTTP);

    //Get the First Day of the Week for Locale - Returns Integer Value
    $firstDayWeek = $intCalendar->getFirstDayOfWeek ();
    //Find cooresponding name / day abbreviation of first day of week
    $dayName = date('D', strtotime("Saturday +{$firstDayWeek} days"));
    //Returns Calendar Type Based on Locale
    $calendarType = $intCalendar->getType();

    //******************************************************//
    //**************Monetary Section ***********************//
    //******************************************************//

    //Check if running on Windows vs Linux
    if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
        //Change underscores with dashes on Windows so setlocale works correctly
        setlocale(3, str_replace('_','-',$localeHTTP));
    } else{
        //Linux uses underscores no need to change string and suffixes .utf8
        setlocale(LC_MONETARY, $localeHTTP . ".utf8");
    }

    //Get Monetary Information Based on Locale - Returns Array
    $monetaryArray = localeconv();

    //International Currency Text Symbol  e.g. USD
    $internationalCurrencySymbol = $monetaryArray['int_curr_symbol'];
    //Currency Symbol

    $formatCurrency = new NumberFormatter($localeHTTP, NumberFormatter::CURRENCY);
    $currencyExample =  $formatCurrency->format(1234567);

    $sourceFile = File::get('assignment1.php');
    $resourceFile = File::get('language.php');


    return view('assignment1')
        ->with('lang',$lang)
        ->with('ipAddress', $ipAddress)
        ->with('sourceFile', $sourceFile)
        ->with('localeHTTP', $localeHTTP)
        ->with('clockFormat', $clockFormat)
        ->with('defaultLocale', $defaultLocale)
        ->with('displayLanguage',$displayLanguage)
        ->with('displayRegion', $displayRegion)
        ->with('displayScript', $displayScript)
        ->with('dayName',$dayName)
        ->with('calendarType', $calendarType)
        ->with('internationalCurrencySymbol', $internationalCurrencySymbol)
        ->with('currencyExample',$currencyExample)
        ->with("resourceFile",$resourceFile)
        ;
});
