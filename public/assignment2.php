<?php


Route::get('/assignment2/{language?}/{region?}', function ($language="en", $region="us") {

    //Web Service Calls

    //Call 1:
    //Returns the list of languages the flower service supports, that is, the list of languages for which we have localized flower, color and occasion names.
    //http://flowerservice20160406025332.azurewebsites.net/languages?languages=en,de,fr,ru
    $client = new GuzzleHttp\Client();
    $result = $client->request('GET', 'http://flowerservice20160406025332.azurewebsites.net/languages?languages=' . $language);
    $json_results = $result->getBody();
    //Array Result
    $flowerLanguages = (json_decode($json_results));
    //dd($flowerLanguages);

    //Call 2:
    //http://flowerservice20160406025332.azurewebsites.net/markets?languages=en,de,fr,ru
    //Returns the list of markets the flower service supports.
    $client2= new GuzzleHttp\Client();
    $result2 = $client2->request('GET', 'http://flowerservice20160406025332.azurewebsites.net/markets?languages=' . $language);
    $json_results2 = $result2->getBody();
    //Array Result
    $flowerMarkets = (json_decode($json_results2));
    //dd($flowerMarkets);

    //Call 3:
    //http://flowerservice20160406025332.azurewebsites.net/flowers?languages=en,de,fr,ru&billingRegion=us
    //Returns the list of flowers available across all markets, along with information about each flower, such as color, description, an image, and price
    $client3= new GuzzleHttp\Client();
    $result3 = $client3->request('GET', 'http://flowerservice20160406025332.azurewebsites.net/flowers?languages=' . $language ."&billingRegion=" . $region);
    $json_results3 = $result3->getBody();
    //Array Result
    $flowerFlowers = (json_decode($json_results3));
    //dd($flowerFlowers);

    //Return View with Information
    return view('assignment2')
        ->with('flowerLanguages', $flowerLanguages)
        ->with('flowerMarkets', $flowerMarkets)
        ->with('flowerFlowers', $flowerFlowers);

});