<?php
/**
 * Created by PhpStorm.
 * User: bk
 * Date: 4/16/2017
 * Time: 11:01 PM
 */

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    'Class' => 'E-37 Programming Exercises',
    'Hello' => 'Hello',
    'Welcome' => 'Welcome',
    'IP' => 'IP Address',
    'Source' => 'Source Code',
    'http_language' => 'HTTP Accept Language',
    'Attribute' => 'Attribute',
    'Value' => 'Value',
    'Homework' => 'Homework Assignment',

    ];