<?php

//Assignment 3 Route
Route::get('/assignment3/{occasionid?}', function ($occasionid=null) {

    //From Assignment 1: I am taking the Primary User Locale and getting the language and country
    //Get abbreviated Locale using the HTTP Header and the acceptFromHTTP Method
    $localeHTTP = Locale::acceptFromHTTP( Request::server('HTTP_ACCEPT_LANGUAGE'));
    //Get country code from Header Locale (e.g. en_US would result in $countryCode[0]=en & $countryCode[1]=US
    $explodedLocale = explode("_", $localeHTTP);


    if(count($explodedLocale) > 1){
        $languageCode = strtolower($explodedLocale[0]);
        $countryCode =  strtolower($explodedLocale[1]);
        if($countryCode == 'gb'){
            $countryCode = 'uk';
        }
    }else{
        $languageCode = $localeHTTP;
        $countryCode = $localeHTTP;

    }


    //dd($countryCode);

    //Updated previous calls to use HTTP Locale Information (Language and Country)
    //Web Service Calls
    //Call 1:
    //Returns the list of languages the flower service supports, that is, the list of languages for which we have localized flower, color and occasion names.
    //http://flowerservice20160406025332.azurewebsites.net/languages?languages=en,de,fr,ru
    $client = new GuzzleHttp\Client();
    $result = $client->request('GET', 'http://flowerservice20160406025332.azurewebsites.net/languages?languages=' . $languageCode);
    $json_results = $result->getBody();
    //Array Result
    $flowerLanguages = (json_decode($json_results));
    //dd($flowerLanguages);

    //Call 2:
    //http://flowerservice20160406025332.azurewebsites.net/markets?languages=en,de,fr,ru
    //Returns the list of markets the flower service supports.
    $client2= new GuzzleHttp\Client();
    $result2 = $client2->request('GET', 'http://flowerservice20160406025332.azurewebsites.net/markets?languages=' . $languageCode);
    $json_results2 = $result2->getBody();
    //Array Result
    $flowerMarkets = (json_decode($json_results2));
    //dd($flowerMarkets);

    //Call 3:
    //http://flowerservice20160406025332.azurewebsites.net/flowers?languages=en,de,fr,ru&billingRegion=us
    //Returns the list of flowers available across all markets, along with information about each flower, such as color, description, an image, and price
    $client3= new GuzzleHttp\Client();
    $result3 = $client3->request('GET', 'http://flowerservice20160406025332.azurewebsites.net/flowers?languages=' . $languageCode ."&billingRegion=" . $countryCode);
    $json_results3 = $result3->getBody();
    //Array Result
    $flowerFlowers = (json_decode($json_results3));
    //dd($flowerFlowers);

    //Call 4:
    //http://flowerservice20160406025332.azurewebsites.net/<marketid>/flowers?languages=en,de,fr,ru&billingRegion=us
    //Returns the list of flowers available in the market that corresponds to the marketid parameter.
    $client4= new GuzzleHttp\Client();
    $result4 = $client4->request('GET', 'http://flowerservice20160406025332.azurewebsites.net/' . $countryCode . '/flowers?languages=' . $languageCode ."&billingRegion=" . $countryCode);
    $json_results4 = $result4->getBody();
    //Array Result
    $flowerMarketAvailable = (json_decode($json_results4));
    //dd($flowerFlowers);

    //Call 5:
    //	http://flowerservice20160406025332.azurewebsites.net/<marketid>/occasions?languages=en,de,fr,ru
    //Returns the list of flowers available in the market that corresponds to the marketid parameter.
    $client5= new GuzzleHttp\Client();
    $result5 = $client5->request('GET', 'http://flowerservice20160406025332.azurewebsites.net/' . $countryCode . '/occasions?languages=' . $languageCode);
    $json_results5 = $result5->getBody();
    //Array Result
    $flowerOccasions = (json_decode($json_results5));

    //Call 6:
    //http://flowerservice20160406025332.azurewebsites.net/uk/21/flowers?languages=en&billingRegion=uk
    //http://flowerservice20160406025332.azurewebsites.net/<marketid>/<occasionid>/flowers?languages=en,de,fr,ru&billingRegion=us
    //Returns the list of flowers available in the market that corresponds to the marketid parameter and the occasionid parameter, along with information about each flower such as color, description, an image, and price.

    $url = 'http://flowerservice20160406025332.azurewebsites.net/' . $countryCode . '/'. $occasionid . '/flowers?languages=' . $languageCode . "&billingRegion=" . $countryCode;
    echo($url);
    $client6= new GuzzleHttp\Client();
    $result6 = $client6->request('GET', $url);
    $json_results6 = $result6->getBody();
    //Array Result
    $flowerOccasionsFlowers = (json_decode($json_results6));

//dd($flowerOccasions);

    //Get Source File to Display in Template
    $sourceFile = File::get('assignment3.php');


    //Return View with Information
    return view('assignment3')
        ->with('flowerLanguages', $flowerLanguages)
        ->with('flowerMarkets', $flowerMarkets)
        ->with('flowerFlowers', $flowerFlowers)
        ->with('flowerMarketAvailable', $flowerMarketAvailable)
        ->with('flowerOccasions', $flowerOccasions)
        ->with('flowerOccasionsFlowers', $flowerOccasionsFlowers)
        ->with('countryCode', $countryCode)
        ->with('languageCode', $languageCode)
        ->with("sourceFile",$sourceFile);
});
