<?php

use Illuminate\Database\Seeder;

class LocalesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $locale = new \App\Locale();
        $locale->locale = "en_US";
        $locale->language = "en";
        $locale->country = "US";
        $locale->displayName = "English - United States";
        $locale->localeNative = array(
            'en_US' => 'English - United States',
            'en_CA' => 'English - United States',
            'fr_CA' => 'États Unis Anglais',
            'ja_JP' => '英語 - アメリカ合衆国',
            'fr_FR' => 'États Unis Anglais',
            'de_DE' => 'Englisch Vereinigte Staaten',
            'es_MX' => 'Inglés Estados Unidos'
        );
        $locale->save();

        $locale = new \App\Locale();
        $locale->locale = "en_CA";
        $locale->language = "en";
        $locale->country = "CA";
        $locale->displayName = "English - Canada";
        $locale->localeNative = array(
            'en_US' => 'English - Canada',
            'en_CA' => 'English - Canada',
            'fr_CA' => 'Anglais - Canada',
            'ja_JP' => '英語 - カナダ',
            'fr_FR' => 'Anglais - Canada',
            'de_DE' => 'Englisch - Kanada',
            'es_MX' => 'Español - Canadá'
        );
        $locale->save();

        $locale = new \App\Locale();
        $locale->locale = "fr_CA";
        $locale->language = "fr";
        $locale->country = "CA";
        $locale->displayName = "French - Canada";
        $locale->localeNative = array(
            'en_US' => 'French - Canada',
            'en_CA' => 'French - Canada',
            'fr_CA' => 'Francés - Canadá',
            'ja_JP' => 'フランス語 - カナダ',
            'fr_FR' => 'Français - Canada',
            'de_DE' => 'Französisch - Kanada',
            'es_MX' => 'Francés - Canadá'
        );
        $locale->save();

        $locale = new \App\Locale();
        $locale->locale = "ja_JP";
        $locale->language = "ja";
        $locale->country = "JP";
        $locale->displayName = "Japanese - Japan";
        $locale->localeNative = array(
            'en_US' => 'Japanese - Japan',
            'en_CA' => 'Japanese - Japan',
            'fr_CA' => 'Japonais - Japon',
            'ja_JP' => '日本 - 日本',
            'fr_FR' => 'Japonais - Japon',
            'de_DE' => 'Japanisch - Japan',
            'es_MX' => 'Japonés - Japón'
        );
        $locale->save();

        $locale = new \App\Locale();
        $locale->locale = "fr_FR";
        $locale->language = "fr";
        $locale->country = "FR";
        $locale->displayName = "French - France";
        $locale->localeNative = array(
            'en_US' => 'French - France',
            'en_CA' => 'French - France',
            'fr_CA' => 'France francaise',
            'ja_JP' => 'フランス - フランス',
            'fr_FR' => 'France francaise',
            'de_DE' => 'Französisch Frankreich',
            'es_MX' => 'Francés Francia'
        );
        $locale->save();

        $locale = new \App\Locale();
        $locale->locale = "de_DE";
        $locale->language = "de";
        $locale->country = "DE";
        $locale->displayName = "German - Germany";
        $locale->localeNative = array(
            'en_US' => 'German - Germany',
            'en_CA' => 'German - Germany',
            'fr_CA' => 'Allemand Allemagne',
            'ja_JP' => 'ドイツ語 - ドイツ',
            'fr_FR' => 'Allemand Allemagne',
            'de_DE' => 'Deutsches Deutschland',
            'es_MX' => 'Alemán Alemania'
        );
        $locale->save();

        $locale = new \App\Locale();
        $locale->locale = "es_MX";
        $locale->language = "es";
        $locale->country = "MX";
        $locale->displayName = "Spanish - Mexico";
        $locale->localeNative = array(
            'en_US' => 'Spanish - Mexico',
            'en_CA' => 'Spanish - Mexico',
            'fr_CA' => 'Espagnol - Mexique',
            'ja_JP' => 'スペイン語 - メキシコ',
            'fr_FR' => 'Espagnol - Mexique',
            'de_DE' => 'Spanisch - Mexiko',
            'es_MX' => 'Español - México'
        );
        $locale->save();


    }
}
