<?php

use Illuminate\Database\Seeder;

class CountriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $country = new \App\Country();
        $country->country = "United States";
        $country->country_code = "US";
        $country->http_accept = array('en_US');
        $country->countryNative = array(
            'en_US' => 'United States',
            'en_CA' => 'United States',
            'fr_CA' => 'États Unis',
            'ja_JP' => 'アメリカ',
            'fr_FR' => 'États Unis',
            'de_DE' => 'Vereinigte Staaten',
            'es_MX' => 'Estados Unidos'
        );
        $country->save();

        $country = new \App\Country();
        $country->country = "Canada";
        $country->country_code = "CA";
        $country->http_accept = array('en_CA', 'fr_CA');
        $country->countryNative = array(
            'en_US' => 'Canada',
            'en_CA' => 'Canada',
            'fr_CA' => 'Canada',
            'ja_JP' => 'カナダ',
            'fr_FR' => 'Canada',
            'de_DE' => 'Kanada',
            'es_MX' => 'Canadá'
        );
        $country->save();

        $country = new \App\Country();
        $country->country = "Japan";
        $country->country_code = "JP";
        $country->http_accept = array('ja_JP');
        $country->countryNative = array(
            'en_US' => 'Japan',
            'en_CA' => 'Japan',
            'fr_CA' => 'Japon',
            'ja_JP' => '日本',
            'fr_FR' => 'Japon',
            'de_DE' => 'Japan',
            'es_MX' => 'Japón'
        );
        $country->save();

        $country = new \App\Country();
        $country->country = "France";
        $country->country_code = "FR";
        $country->http_accept = array('fr_FR');
        $country->countryNative = array(
            'en_US' => 'France',
            'en_CA' => 'France',
            'fr_CA' => 'France',
            'ja_JP' => 'フランス',
            'fr_FR' => 'France',
            'de_DE' => 'Frankreich',
            'es_MX' => 'Francia'
        );
        $country->save();

        $country = new \App\Country();
        $country->country = "Germany";
        $country->country_code = "DE";
        $country->http_accept = array('de_DE');
        $country->countryNative = array(
            'en_US' => 'Germany',
            'en_CA' => 'Germany',
            'fr_CA' => 'Allemagne',
            'ja_JP' => 'ドイツ',
            'fr_FR' => 'Allemagne',
            'de_DE' => 'Deutschland',
            'es_MX' => 'Alemania'
        );
        $country->save();

        $country = new \App\Country();
        $country->country = "Mexico";
        $country->country_code = "MX";
        $country->http_accept = array('es_MX');
        $country->countryNative = array(
            'en_US' => 'Mexico',
            'en_CA' => 'Mexico',
            'fr_CA' => 'Mexique',
            'ja_JP' => 'メキシコ',
            'fr_FR' => 'Mexique',
            'de_DE' => 'Mexiko',
            'es_MX' => 'Méjico'
        );
        $country->save();
    }

}
