<?php

use Illuminate\Database\Seeder;

class CurrenciesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $currency = new \App\Currency();
        $currency->currency = "Peso";
        $currency->currency_symbol = "MXN$";
        $currency->currency_code = "MXN";
        $currency->http_accept = array('es_MX');
        $currency->country = array('MX');
        $currency->save();

        $currency = new \App\Currency();
        $currency->currency = "Euro";
        $currency->currency_symbol = "€";
        $currency->currency_code = "EUR";
        $currency->http_accept = array('fr_FR', 'de_DE');
        $currency->country = array('FR','DE');
        $currency->save();

        $currency = new \App\Currency();
        $currency->currency = "Dollar";
        $currency->currency_symbol = "CDN$";
        $currency->currency_code = "CAD";
        $currency->http_accept = array('en_CA', 'fr_CA');
        $currency->country = array('CA');
        $currency->save();

        $currency = new \App\Currency();
        $currency->currency = "Dollar";
        $currency->currency_symbol = "USD$";
        $currency->currency_code = "USD";
        $currency->http_accept = array('en_US');
        $currency->country = array('US');
        $currency->save();

        $currency = new \App\Currency();
        $currency->currency = "Yen";
        $currency->currency_symbol = "¥";
        $currency->currency_code = "JPY";
        $currency->http_accept = array('ja_JP');
        $currency->country = array('JP');
        $currency->save();


    }
}
