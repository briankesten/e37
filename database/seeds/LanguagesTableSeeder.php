<?php

use Illuminate\Database\Seeder;

class LanguagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $language = new \App\Language();
        $language->language = "English";
        $language->languageNative = array(
            'en_US' => 'English',
            'en_CA' => 'English',
            'fr_CA' => 'Anglais',
            'ja_JP' => '英語',
            'fr_FR' => 'Anglais',
            'de_DE' => 'Englisch',
            'es_MX' => 'Inglés'
        );
        $language->language_code = "en";
        $language->http_accept = array('en_CA', 'en_US');
        $language->save();


        $language = new \App\Language();
        $language->language = "Deutsch";
        $language->languageNative = array(
            'en_US' => 'German',
            'en_CA' => 'German',
            'fr_CA' => 'allemand',
            'ja_JP' => 'ドイツ語',
            'fr_FR' => 'allemand',
            'de_DE' => 'Deutsche',
            'es_MX' => 'alemán'
        );
        $language->language_code = "de";
        $language->http_accept = array("de_DE");
        $language->save();

        $language = new \App\Language();
        $language->language = "Japanese";
        $language->languageNative = array(
            'en_US' => 'Japanese',
            'en_CA' => 'Japanese',
            'fr_CA' => 'Japonais',
            'ja_JP' => '日本語',
            'fr_FR' => 'Japonais',
            'de_DE' => 'japanisch',
            'es_MX' => 'japonés'
        );
        $language->language_code = "ja";
        $language->http_accept = array("ja_JP", "ja");
        $language->save();

        $language = new \App\Language();
        $language->language = "Spanish";
        $language->languageNative = array(
            'en_US' => 'Spanish',
            'en_CA' => 'Spanish',
            'fr_CA' => 'Espanol',
            'ja_JP' => 'スペイン語',
            'fr_FR' => 'Espanol',
            'de_DE' => 'Spanisch',
            'es_MX' => 'Español'
        );
        $language->language_code = "es";
        $language->http_accept = array("es_MX");
        $language->save();

        $language = new \App\Language();
        $language->language = "French";
        $language->languageNative = array(
            'en_US' => 'French',
            'en_CA' => 'French',
            'fr_CA' => 'français',
            'ja_JP' => 'フランス語',
            'fr_FR' => 'français',
            'de_DE' => 'Französisch',
            'es_MX' => 'francés'
        );
        $language->language_code = "fr";
        $language->http_accept = array("fr_CA", "fr_FR");
        $language->save();

    }
}
