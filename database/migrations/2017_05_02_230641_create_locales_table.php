<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateLocalesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('locales', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->string('displayName');
            $table->string('locale');
            $table->string('language');
            $table->string('country');
            $table->text('localeNative');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('locales');
    }
}
