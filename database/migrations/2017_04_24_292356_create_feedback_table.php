<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFeedbackTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        //
        Schema::create('feedback', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            // User Info
            $table->string('name');
            $table->string('email');
            $table->string('company');
            //Feedback
            $table->text('feedback');
            $table->text('subject');
           // $table->date('date');
            $table->text('amount');

            //Foreign Keys
            $table->integer('language_id')->unsigned();
            $table->integer('country_id')->unsigned();
            $table->integer('currency_id')->unsigned();

            # Foreign Keys - Don't Delete records if user is deleted
            $table->foreign('language_id')->references('id')->on('languages');
            $table->foreign('country_id')->references('id')->on('countries');
            $table->foreign('currency_id')->references('id')->on('currencies');
        });
    }

    /**
     * Reverse the migrations.3
     *
     * @return void
     */
    public function down()
    {
        //
        Schema::dropIfExists('feedback');
    }
}
