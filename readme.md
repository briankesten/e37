

## E-37 Coding Excercise Repository
E-37 Coding Excercises were developed with PHP using the Laravel Framework.   

Below is the list of the main areas to find the full code for the E-37 Coding Project:

- [Final Project Main Code Feedback Controller](https://bitbucket.org/briankesten/e37/src/1c000f91e07311766825b2a3ee005b9e5f0edb5d/app/Http/Controllers/FeedbackController.php?at=master&fileviewer=file-view-default).
- [Final Project Blade Views](https://bitbucket.org/briankesten/e37/src/1c000f91e07311766825b2a3ee005b9e5f0edb5d/resources/views/feedback/?at=master).
- [Final Project Database Migrations](https://bitbucket.org/briankesten/e37/src/1c000f91e07311766825b2a3ee005b9e5f0edb5d/database/migrations/?at=master).
- [Final Project Database Seeders](https://bitbucket.org/briankesten/e37/src/1c000f91e07311766825b2a3ee005b9e5f0edb5d/database/seeds/?at=master).
- [Assignment 4 Controller - Core code for Assignment 4](https://bitbucket.org/briankesten/e37/src/dc982f792ad03c2e4245f98d249ccf7da6313379/app/Http/Controllers/FlowerController.php?at=master&fileviewer=file-view-default).
- [Assignment 4 Localization Directories - Look for flowers.php](https://bitbucket.org/briankesten/e37/src/dc982f792ad03c2e4245f98d249ccf7da6313379/resources/lang/?at=master).
- [Assignment 4 View - Contains UX details](https://bitbucket.org/briankesten/e37/src/dc982f792ad03c2e4245f98d249ccf7da6313379/resources/views/assignment4.blade.php?at=master&fileviewer=file-view-default).
- [Assignment 1 - 3 Core Code](https://bitbucket.org/briankesten/e37/src/dc982f792ad03c2e4245f98d249ccf7da6313379/routes/web.php?at=master&fileviewer=file-view-default).
- [Assignment 1 - 3 Views - UX Design Code](https://bitbucket.org/briankesten/e37/src/dc982f792ad03c2e4245f98d249ccf7da6313379/resources/views/?at=master).

## Dependencies 
- GuzzleHTTP - http://docs.guzzlephp.org/en/latest/
- LaravelCollective - https://laravelcollective.com/

