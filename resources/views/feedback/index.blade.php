@extends('layouts.master')

@section('bodycontent')
    <br><br>
    <div class="row">
        <div class="col-sm-12">
            @isset($localeDropDown)
                <div class="dropdown">
                    <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="true">
                        <span class="glyphicon glyphicon-globe" aria-hidden="true"></span>
                        @lang('feedback.supportedlocale')
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                        @foreach($localeDropDown as $locale)
                            <li><a href="?getlocale={{ $locale['language'] }}&getcountry={{$locale['country']}}&getfulllocale={{$locale['locale']}}">{{$locale['full_text_locale']}}</a></li>
                        @endforeach
                    </ul>
                </div>
            @endisset
        </div>
    </div>
    <br><br>
    @foreach ($feedback as $feed)
        <div class="row">
            <div class="col-xs-3">
                {{$feed->id }}
            </div>
            <div class="col-xs-3">
                {{$feed->subject }}
            </div>
            <div class="col-xs-3">
                {{$feed->feedback }}
            </div>
            <div class="col-xs-3">
                <a href="/feedback/{{$feed->id}}/edit">@lang('feedback.edit')</a><br>
            </div>
        </div>
    @endforeach


@endsection

