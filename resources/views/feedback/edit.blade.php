@extends('layouts.master')

@section('bodycontent')
    <br><br>
    <div class="row">
        <div class="col-sm-12">
            @isset($localeDropDown)
                <div class="dropdown">
                    <button class="btn btn-default dropdown-toggle" type="button" id="dropdownMenu1" data-toggle="dropdown"
                            aria-haspopup="true" aria-expanded="true">
                        <span class="glyphicon glyphicon-globe" aria-hidden="true"></span>
                        @lang('feedback.supportedlocale')
                        <span class="caret"></span>
                    </button>
                    <ul class="dropdown-menu" aria-labelledby="dropdownMenu1">
                        @foreach($localeDropDown as $locale)
                            <li><a href="?getlocale={{ $locale['language'] }}&getcountry={{$locale['country']}}&getfulllocale={{$locale['locale']}}">{{$locale['full_text_locale']}}</a></li>
                        @endforeach
                    </ul>
                </div>
            @endisset
        </div>
    </div>
    <br><br>
    {!!  Form::model($feedback, ['route' => ['feedback.update', $feedback->id]]) !!}
    {!! csrf_field() !!}
    {!! method_field('PUT') !!}
    <div class="row">
        <div class="col-sm-6">
            <h1>@lang('feedback.submitfeedback')</h1>

            <div class="form-group">
                <label for="feedbackSelectLanguage">@lang('feedback.language')</label>
                <select name="language_id" id="feedbackSelectLanguage" class="form-control">
                    @foreach($languageCollect as $lancollect)
                            @if($lancollect['language_id'] == old('language_id'))
                                <option value=" {{ $lancollect['language_id'] }}" selected>
                                    {{ $lancollect['language'] }}
                                </option>
                            @elseif($lancollect['language_id'] == $feedback->language_id)
                                <option value="{{ $lancollect['language_id'] }}" selected>
                                    {{ $lancollect['language'] }}
                                </option>
                             @else
                                <option value="{{ $lancollect['language_id'] }}">
                                    {{ $lancollect['language'] }}
                                </option>
                        @endif
                    @endforeach
                </select>
            </div>

            <div class="form-group">
                <label for="feedbackSelectCountry">@lang('feedback.country')</label>
                <select name="country_id" id="feedbackSelectCountry" class="form-control">
                    @foreach($countryCollect as $collect)
                        @if($collect['country_id'] == old('country_id'))
                            <option value=" {{ $collect['country_id'] }}" selected>
                                {{ $collect['country'] }}
                            </option>
                        @elseif($collect['country_id'] == $feedback->country_id)
                            <option value="{{ $collect['country_id'] }}" selected>
                                {{ $collect['country'] }}
                            </option>
                        @else
                            <option value="{{ $collect['country_id'] }}">
                                {{ $collect['country'] }}
                            </option>
                        @endif
                    @endforeach
                </select>
            </div>

            @if($finalLocale[0] == "fr_FR" || $finalLocale[0] == "de_DE" || $finalLocale[0] =="fr_CA")
                @include('partial.currencysymbolafter')
            @else
                @include('partial.currencysymbolbefore')
            @endif

            <div class="form-group">
                <label for="feedbackInputCompany">@lang('feedback.company')</label>
                <input name="company" type="text" class="form-control" id="feedbackInputCompany" value="{{ $feedback->company }}"
                       placeholder="@lang('feedback.company_placeholder')">
            </div>
            <div class="form-group">
                <label for="feedbackInputEmail">@lang('feedback.email')</label>
                <input name="email" type="email" class="form-control" id="feedbackInputEmai1" value="{{ $feedback->email }}"
                       placeholder="@lang('feedback.email_placeholder')">
            </div>
            <div class="form-group">
                <label for="feedbackInputName">@lang('feedback.name')</label>
                <input name="name" type="text" class="form-control" id="feedbackInputName" value="{{ $feedback->name }}"
                       placeholder="@lang('feedback.name_placeholder')">
            </div>
            <div class="form-group">
                <label for="feedbackInputSubject">@lang('feedback.subject')</label>
                <input name="subject" type="text" class="form-control" id="feedbackInputSubject" value="{{ $feedback->subject }}"
                       placeholder="@lang('feedback.subject_placeholder')">
            </div>
            <div class="form-group">
                <label for="feedbackInputFeedback">@lang('feedback.feedback')</label>
                <textarea name="feedback" id="feedbackInputFeedback" placeholder="@lang('feedback.feedback')"
                          class="form-control"
                          rows="3"> {{ $feedback->feedback }} </textarea>
            </div>
            <button type="submit" class="btn btn-default">@lang('feedback.submitfeedback')</button>


            {!! Form::close() !!}
        </div>
        <div class="col-sm-6">

        </div>
    </div>
@endsection

