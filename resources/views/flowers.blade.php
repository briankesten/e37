<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>@lang('flowers.title')</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">


    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <!-- Latest compiled and minified JavaScript -->
    <script
            src="https://code.jquery.com/jquery-3.2.1.min.js"
            integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
            crossorigin="anonymous"></script>

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>


</head>
<body>
<div class="container">
    <div>
        <img src="/img/logo.png">
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">@lang('flowers.title')</h3>
            </div>
            <div class="panel-body">
                @if (count($errors) > 0)
                    <div class="alert alert-danger">
                        <ul>
                            @foreach ($errors->all() as $error)
                                <li>{{ $error }}</li>
                            @endforeach
                        </ul>
                    </div>
                @endif

                <div class="row">
                    {!! Form::open(['url' => '/flowers', 'method' => 'post']) !!}
                    <div class="col-sm-3">
                        <h4>@lang('flowers.language')</h4>
                        <select name="languageCode" class="form-control">
                            @foreach($flowerLanguages as $fl)
                                <option value="{{$fl->LanguageId}}" {{ $languageCode == $fl->LanguageId ? "selected" : '' }}>{{$fl->NativeName}}</option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-sm-3">
                        <h4>@lang('flowers.destination')</h4>
                        <select name="countryCode" class="form-control">
                            @foreach($flowerMarkets as $fm)
                                <option value="{{$fm->MarketId}}" {{ $countryCode == $fm->MarketId ? "selected" : '' }}>
                                    @for($i = 0; $i < count($fm->MarketName) ; $i++)
                                        {{ $fm->MarketName[$i]->Key == $languageCode ? $fm->MarketName[$i]->Value: null }}
                                    @endfor
                                </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-sm-3">
                        <h4>@lang('flowers.occasion')</h4>
                        <select name="occasionId" class="form-control">
                            <option value="0">@lang('flowers.alloccasions')</option>
                            @foreach($flowerOccasions as $fo)
                                <option value="{{$fo->OccasionId}}" {{ $occasionId == $fo->OccasionId ? "selected" : '' }}>
                                    @for($i = 0; $i < count($fo->DisplayName) ; $i++)
                                        {{ $fo->DisplayName[$i]->Key == $languageCode ? $fo->DisplayName[$i]->Value: null }}
                                    @endfor
                                </option>
                            @endforeach
                        </select>
                    </div>
                    <div class="col-sm-3">
                        <h4>@lang('flowers.search')</h4>
                        <button class="btn btn-primary" type="submit">@lang('flowers.update')</button>
                    </div>
                    {!! Form::close() !!}
                </div>
                <div class="row">
                    <div class="col-sm-12"><h1></h1></div>
                </div>
                <div class="container">
                    @foreach($flowerResults as $fma)
                        <div class="row">
                            <div class="col-sm-3">
                                <img src="http://flowerservice20160406025332.azurewebsites.net/{{$fma->ImageUri}}">
                            </div>
                            <div class="col-sm-3">
                                <h5> @lang('flowers.colors')</h5>
                                @for($i = 0; $i < count($fma->Colors) ; $i++)
                                    <img src="{{$colorSamples[$fma->Colors[$i]]}}" class="img-thumbnail">
                                @endfor
                            </div>
                            <div class="col-sm-3">
                                @for($i = 0; $i < count($fma->DisplayName) ; $i++)
                                    {{ $fma->DisplayName[$i]->Key == $languageCode ? $fma->DisplayName[$i]->Value: null }}
                                @endfor
                                   ( {{$fma->GlobalDisplayName}})
                            </div>
                            <div class="col-sm-3">
                                {!! $fma->PriceInfoOfCurrentBillingRegion->Currency !!}
                                {{  $fma->PriceInfoOfCurrentBillingRegion->Price}}
                                {{  $fma->PriceInfoOfCurrentBillingRegion->MarketId}}
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
</div>
</div>
</body>
</html>
