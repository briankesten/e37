<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Module 3: Assignment 1</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">


    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <!-- Latest compiled and minified JavaScript -->
    <script
            src="https://code.jquery.com/jquery-3.2.1.min.js"
            integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
            crossorigin="anonymous"></script>

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>

</head>
<body>
<div class="position-ref full-height">
    @if (Route::has('login'))
        <div class="top-right links">
            @if (Auth::check())
                <a href="{{ url('/home') }}">Home</a>
            @else
                <a href="{{ url('/login') }}">Login</a>
                <a href="{{ url('/register') }}">Register</a>
            @endif
        </div>
    @endif

    <div>
        <h1>
            @lang('test.Class')
        </h1>

        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Module 3: @lang('test.Homework') 1</h3>
            </div>
            <div class="panel-body">
                <div class="alert alert-success" role="alert"><strong> Note: </strong> Added some translations to resource file.  You can see the page change by using the following URL:.  E.g. <br>
                    <a href="/assignment1/es">Spanish URL Language Example (Limited Translations)</a><br>
                    <a href="/assignment1">English Default URL Language Example</a><br>
                </div>
                <table class="table">
                    <thead>
                    <tr>
                        <th>@lang('test.Attribute')</th>
                        <th>@lang('test.Value')</th>
                    </tr>
                    </thead>
                    <tbody>
                    <tr>
                        <td>@lang('test.http_language')</td>
                        <td>{{ $lang }}</td>
                    </tr>
                    <tr>
                        <td>@lang('test.IP')</td>
                        <td>{{$ipAddress}}</td>
                    </tr>
                    <tr>
                        <td>HTTP Header Locale Information(Abbrev)</td>
                        <td>{{$localeHTTP}}</td>
                    </tr>
                    <tr>
                        <td>Locale Clock Format</td>
                        <td>{{$clockFormat}}</td>
                    </tr>
                    <tr>
                        <td>Default Display Locale</td>
                        <td>{{$defaultLocale}}</td>
                    </tr>
                    <tr>
                        <td>Display Language</td>
                        <td>{{$displayLanguage}}</td>
                    </tr>
                    <tr>
                        <td>Display Region</td>
                        <td>{{$displayRegion}}</td>
                    </tr>
                    <tr>
                        <td>Display Script</td>
                        <td>{{$displayScript}}</td>
                    </tr>
                    <tr>
                        <td>First Day of the Week</td>
                        <td>{{$dayName}}</td>
                    </tr>
                    <tr>
                        <td>Calendar Type</td>
                        <td>{{$calendarType}}</td>
                    </tr>
                    <tr>
                        <td>International Currency Symbol</td>
                        <td>{{ $internationalCurrencySymbol}}</td>
                    </tr>
                    <tr>
                        <td>Currency formatting example of 1234567</td>
                        <td>{{$currencyExample}}</td>
                    </tr>

                    </tbody>
                </table>
            </div>
        </div>


        <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
            <div class="panel panel-primary">
                <div class="panel-heading" role="tab" id="headingOne">
                    <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne"
                           aria-expanded="false" aria-controls="collapseOne">
                            @lang('test.Source')
                        </a>
                    </h4>
                </div>
                <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
                    <div class="panel-body">
                        <pre>{{ $sourceFile }}</pre>
                    </div>
                </div>
            </div>
        </div>

        <div class="panel-group" id="accordion5" role="tablist" aria-multiselectable="true">
            <div class="panel panel-primary">
                <div class="panel-heading" role="tab" id="headingFive">
                    <h4 class="panel-title">
                        <a role="button" data-toggle="collapse" data-parent="#accordion5" href="#collapseFive"
                           aria-expanded="false" aria-controls="collapseFive">
                            Resource File
                        </a>
                    </h4>
                </div>
                <div id="collapseFive" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFive">
                    <div class="panel-body">
                         <pre>{{ $resourceFile }}</pre>
                    </div>
                </div>
            </div>
        </div>


    </div>

</div>
</body>
</html>
