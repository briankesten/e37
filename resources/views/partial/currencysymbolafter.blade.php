<div class="form-group">
    <label for="feedbackSelectCountry">@lang('feedback.price')  </label>
    <div class="row">
        <div class="col-xs-8">
            <input name="amount" type="text" class="form-control" id="feedbackInputCompany" value="{{isset($feedback->amount) ? $feedback->amount : ""}}"
                   placeholder="{{ $currencyPlaceholder }}">
        </div>
        <div class="col-xs-4">
            <select name="currency_id" id="feedbackSelectCountry" class="form-control">
                <!-- Set Dropdown List - save old value if submitted form and failed -->
                @foreach($currencies as $currency)
                    @if(old('currency_id'))
                        @if($currency->id == old('currency_id'))
                            <option value="{{ $currency->id}}"
                                    selected>{{$currency->currency_symbol}}
                            </option>
                        @else
                            <option value="{{ $currency->id }}">{{$currency->currency_symbol}}
                            </option>
                        @endif
                    @elseif(isset($feedback->currency_id))
                        <option value="{{ $currency->id }}" {{ ($currency->id == $feedback->currency_id) ? "selected" : "" }}>
                            {{$currency->currency_symbol}}
                        </option>
                    @else
                        <option value="{{ $currency->id }}" {{ (in_array($finalLocale[0], $currency->http_accept) ? "selected" : "")  }}>
                            {{$currency->currency_symbol}}
                        </option>
                    @endif
                @endforeach
            </select>
        </div>
    </div>

</div>