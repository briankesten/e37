<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Module 3: Assignment 3</title>

    <!-- Fonts -->
    <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">


    <!-- Latest compiled and minified CSS -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
          integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">
    <!-- Latest compiled and minified JavaScript -->
    <script
            src="https://code.jquery.com/jquery-3.2.1.min.js"
            integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
            crossorigin="anonymous"></script>

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous"></script>


</head>
<body>
<div class="position-ref full-height">
    @if (Route::has('login'))
        <div class="top-right links">
            @if (Auth::check())
                <a href="{{ url('/home') }}">Home</a>
            @else
                <a href="{{ url('/login') }}">Login</a>
                <a href="{{ url('/register') }}">Register</a>
            @endif
        </div>
    @endif

    <div>
        <h1>
            @lang('test.Class')
        </h1>

        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Module 3: @lang('test.Homework') 3 Part 1</h3>
            </div>
            <div class="panel-body">
                <div class="alert alert-success" role="alert"><strong> Note: </strong> Utilizing the region and language
                    from the user's HTTP header information that was used in Assignment 1. I verified that switching
                    browser languge seetings will change the result of the API calls. However, I noticed that the Chrome
                    browser didn't return the expected country code and I had to handle this via software code.</a><br>
                </div>

                <div class="panel-group" id="accordion2" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-primary">
                        <div class="panel-heading" role="tab" id="headingTwo">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo"
                                   aria-expanded="false" aria-controls="collapseOne">
                                    Flower Language API Call Information
                                </a>
                            </h4>
                        </div>
                        <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel"
                             aria-labelledby="headingTwo">
                            <div class="panel-body">
                                <pre>
                                  @php(print_r($flowerLanguages))@endphp
                             </pre>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-group" id="accordion3" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-primary">
                        <div class="panel-heading" role="tab" id="headingThree">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion3" href="#collapseThree"
                                   aria-expanded="false" aria-controls="collapseThree">
                                    Flower Market API Call Information
                                </a>
                            </h4>
                        </div>
                        <div id="collapseThree" class="panel-collapse collapse" role="tabpanel"
                             aria-labelledby="headingThree">
                            <div class="panel-body">
                                <pre>
                                   @php(print_r($flowerMarkets))@endphp
                             </pre>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-group" id="accordion4" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-primary">
                        <div class="panel-heading" role="tab" id="headingFour">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion4" href="#collapseFour"
                                   aria-expanded="false" aria-controls="collapseFour">
                                    Flower API Call Information
                                </a>
                            </h4>
                        </div>
                        <div id="collapseFour" class="panel-collapse collapse" role="tabpanel"
                             aria-labelledby="headingFour">
                            <div class="panel-body">
                                <pre>
                                   @php(print_r($flowerFlowers))@endphp
                             </pre>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-primary">
                        <div class="panel-heading" role="tab" id="headingOne">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapseOne"
                                   aria-expanded="false" aria-controls="collapseOne">
                                    @lang('test.Source')
                                </a>
                            </h4>
                        </div>
                        <div id="collapseOne" class="panel-collapse collapse" role="tabpanel"
                             aria-labelledby="headingOne">
                            <div class="panel-body">
                                <pre>{{ $sourceFile }}</pre>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="panel panel-primary">
            <div class="panel-heading">
                <h3 class="panel-title">Module 3: @lang('test.Homework') 3 Part 2</h3>
            </div>
            <div class="panel-body">
                <div class="alert alert-success" role="alert"><strong> Note: </strong> Assignment 3 part 2 utlizes 3 new APIs.  Use the links in the, Occasions by Market API Call Information, to pass different occasions into the last API.</a><br>
                </div>
                <div class="panel-group" id="accordion6" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-primary">
                        <div class="panel-heading" role="tab" id="headingSix">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion6" href="#collapseSix"
                                   aria-expanded="false" aria-controls="collapseSix">
                                    Flowers Available in Market API Call Information
                                </a>
                            </h4>
                        </div>
                        <div id="collapseSix" class="panel-collapse collapse" role="tabpanel"
                             aria-labelledby="headingSix">
                            <div class="panel-body">
                                <pre>
                                  @php(print_r($flowerMarketAvailable))@endphp
                             </pre>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-group" id="accordion7" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-primary">
                        <div class="panel-heading" role="tab" id="headingSeven">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion7" href="#collapseSeven"
                                   aria-expanded="false" aria-controls="collapseSeven">
                                    Occasions by Market API Call Information
                                </a>
                            </h4>
                        </div>
                        <div id="collapseSeven" class="panel-collapse collapse" role="tabpanel"
                             aria-labelledby="headingSeven">
                            <div class="panel-body">
                                <pre>
                                  @php(print_r($flowerOccasions))@endphp
                             </pre>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel-group" id="accordion8" role="tablist" aria-multiselectable="true">
                    <div class="panel panel-primary">
                        <div class="panel-heading" role="tab" id="headingEight">
                            <h4 class="panel-title">
                                <a role="button" data-toggle="collapse" data-parent="#accordion8" href="#collapseEight"
                                   aria-expanded="false" aria-controls="collapseEight">
                                    Flowers availabe by Market and Occasion
                                </a>
                            </h4>
                        </div>
                        <div id="collapseEight" class="panel-collapse collapse" role="tabpanel"
                             aria-labelledby="headingEight">
                            <div class="panel-body">
                                <h5>Occasion Loop</h5>
                                @foreach($flowerOccasions as $flowerOccasion)
                                    <a href="/assignment3/{{$flowerOccasion->OccasionId}}">Occasion Link {{ $flowerOccasion->OccasionId }}</a><br>
                                @endforeach
                                <br>
                                <pre>
                                  @php(print_r($flowerOccasionsFlowers))@endphp
                             </pre>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
