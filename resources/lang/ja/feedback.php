<?php
/**
 * Created by PhpStorm.
 * User: bk
 * Date: 4/24/2017
 * Time: 10:46 PM
 */

return [

    /*
    |--------------------------------------------------------------------------
    | Feedback Language Lines
    |--------------------------------------------------------------------------
    |
    |
    |
    */
    'home' => 'ホーム',
    'title' => 'UserGenieのフィードバックページ',
    'language' => '言語を選択する',
    'country' => '国を選択',
    'amount' => '金額を入力してください',
    'feedback' => 'フィードバックを入力',
    'email' => 'メールアドレスを入力',
    'email_placeholder' => 'メールアドレスを入力',
    'name' => '名',
    'name_placeholder' => 'あなたの名前を入力してください',
    'date' => 'フィードバックの日付を入力してください',
    'company' => '会社',
    'company_placeholder' => 'あなたの会社を入力してください',
    'subject' => 'タイトル',
    'subject_placeholder' => 'フィードバックのタイトルを入力してください',
    'price' => '製品に支払った価格',
    'submitfeedback' => 'フィードバックを送信',
    'edit' => '編集',
    'supportedlocale' => 'サポートされているロケールを選択',
];