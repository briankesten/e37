<?php
/**
 * Created by PhpStorm.
 * User: bk
 * Date: 4/16/2017
 * Time: 11:01 PM
 */

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'Class' => 'E-37 Ejercicios de programación',
    'Hello' => 'Hola',
    'Welcome' => 'Bienvenido',
    'IP' => 'dirección IP',
    'Source' => 'Código fuente',
    'http_language' => 'HTTP Aceptar idioma',
    'Attribute' => 'Atributo',
    'Value' => 'Valor',
    'Homework' => 'Asignación de tareas',

];