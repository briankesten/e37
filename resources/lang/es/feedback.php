<?php
/**
 * Created by PhpStorm.
 * User: bk
 * Date: 4/24/2017
 * Time: 10:46 PM
 */

return [

    /*
    |--------------------------------------------------------------------------
    | Feedback Language Lines
    |--------------------------------------------------------------------------
    |
    |
    |
    */
    'home' => 'CASA',
    'title' => 'Página de comentarios de UserGenie',
    'language' => 'Seleccione el idioma',
    'country' => 'Seleccionar país',
    'amount' => 'Ingrese una cantidad',
    'feedback' => 'Introducir comentarios',
    'email' => 'Introduce tu correo electrónico',
    'email_placeholder' => 'Introduce tu correo electrónico',
    'name' => 'Nombre',
    'name_placeholder' => 'Introduzca su nombre',
    'date' => 'Introduzca la fecha para sus comentarios',
    'company' => 'Empresa',
    'company_placeholder' => 'Ingrese su empresa',
    'subject' => 'Título',
    'subject_placeholder' => 'Introduce un título para tus comentarios',
    'price' => 'Precio que pagó por el producto',
    'submitfeedback' => 'Enviar comentarios',
    'edit' => 'Editar',
    'supportedlocale' => 'Selecciona la configuración regional soportada',
];