<?php


return [

    /*
    |--------------------------------------------------------------------------
    | Flower Service Language Lines
    |--------------------------------------------------------------------------
    |
    |
    |
    */
    'title' => 'Flower Information',
    'language' => 'Language',
    'destination' => 'Destination',
    'occasion' => 'Occasion',
    'search' => 'Search',
    'update' => 'Update',
    'alloccasions' => 'All Occasions',
    'colors' => 'Colors',

];