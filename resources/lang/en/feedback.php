<?php
/**
 * Created by PhpStorm.
 * User: bk
 * Date: 4/24/2017
 * Time: 10:46 PM
 */

return [

    /*
    |--------------------------------------------------------------------------
    | Feedback Language Lines
    |--------------------------------------------------------------------------
    |
    |
    |
    */
    'home' => 'Home',
    'title' => 'UserGenie Feedback Page',
    'language' => 'Select Language',
    'country' => 'Select Country',
    'amount' => 'Enter an Amount',
    'feedback' => 'Enter Feedback',
    'email' => 'Enter your email',
    'email_placeholder' => 'Enter your email',
    'name' => 'Name',
    'name_placeholder' => 'Enter your name',
    'date' => 'Enter date for your feedback',
    'company' => 'Company',
    'company_placeholder' => 'Enter your company',
    'subject' => 'Title',
    'subject_placeholder' => 'Enter a title for your feedback',
    'price' => 'Price you paid for the product',
    'submitfeedback' => 'Submit Feedback',
    'edit' => 'Edit',
    'supportedlocale' => 'Select supported locale',
];