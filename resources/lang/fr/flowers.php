<?php


return [

    /*
    |--------------------------------------------------------------------------
    | Flower Service Language Lines
    |--------------------------------------------------------------------------
    |
    |
    |
    */
    'title' => 'Information sur les fleurs',
    'language' => 'La langue',
    'destination' => 'Expédition à',
    'occasion' => 'Occasion',
    'search' => 'Chercher',
    'update' => 'Mettre à jour',
    'alloccasions' => 'Toutes les occasions',
    'colors' => 'Couleurs',

];