<?php
/**
 * Created by PhpStorm.
 * User: bk
 * Date: 4/24/2017
 * Time: 10:46 PM
 */

return [

    /*
    |--------------------------------------------------------------------------
    | Feedback Language Lines
    |--------------------------------------------------------------------------
    |
    |
    |
    */
    'home' => 'MAISON',
    'title' => 'Page Feedback de UserGenie',
    'language' => 'Choisir la langue',
    'country' => 'Sélectionner le pays',
    'amount' => 'Entrez un montant',
    'feedback' => 'Entrez les commentaires',
    'email' => 'Entrer votre Email',
    'email_placeholder' => 'Entrer votre Email',
    'name' => 'prénom',
    'name_placeholder' => 'Entrez votre nom',
    'date' => 'Entrez la date pour vos commentaires',
    'company' => 'Compagnie',
    'company_placeholder' => 'Entrez votre entreprise',
    'subject' => 'Titre',
    'subject_placeholder' => 'Entrez un titre pour vos commentaires',
    'price' => 'Prix ​​que vous avez payé pour le produit',
    'submitfeedback' => 'Soumettre des commentaires',
    'edit' => 'modifier',
    'supportedlocale' => 'Sélectionnez les paramètres régionaux pris en charge',
];