<?php
/**
 * Created by PhpStorm.
 * User: bk
 * Date: 4/24/2017
 * Time: 10:46 PM
 */

return [

    /*
    |--------------------------------------------------------------------------
    | Feedback Language Lines
    |--------------------------------------------------------------------------
    |
    |
    |
    */
    'home' => 'ZUHAUSE',
    'title' => 'UserGenie Feedback Seite',
    'language' => 'Sprache auswählen',
    'country' => 'Land auswählen',
    'amount' => 'Geben Sie einen Betrag ein',
    'email' => 'Geben sie ihre E-Mail Adresse ein',
    'email_placeholder' => 'Geben sie ihre E-Mail Adresse ein',
    'name' => 'Name',
    'name_placeholder' => 'Gib deinen Namen ein',
    'date' => 'Geben Sie das Datum für Ihr Feedback ein',
    'company' => 'Unternehmen',
    'company_placeholder' => 'Geben Sie Ihre Firma ein',
    'subject' => 'Titel',
    'subject_placeholder' => 'Geben Sie einen Titel für Ihr Feedback ein',
    'price' => 'Preis, den Sie für das Produkt bezahlt haben',
    'feedback' => 'Geben Sie Feedback ein',
    'submitfeedback' => 'Feedback abgeben',
    'edit' => 'Bearbeiten',
    'supportedlocale' => 'Wählen Sie das unterstützte Gebietsschema',
];