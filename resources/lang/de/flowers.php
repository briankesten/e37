<?php


return [

    /*
    |--------------------------------------------------------------------------
    | Flower Service Language Lines
    |--------------------------------------------------------------------------
    |
    |
    |
    */
    'title' => 'Blumeninformationen',
    'language' => 'Sprache',
    'destination' => 'Reiseziel',
    'occasion' => 'Gelegenheit',
    'search' => 'Suche',
    'update' => 'Aktualisieren',
    'alloccasions' => 'Alle Anlässe',
    'colors' => 'Farben',
];