<?php

namespace App\Http\Controllers;

use App\Country;
use App\Currency;
use App\Feedback;
use App\Language;
use Illuminate\Http\Request;
use NumberFormatter;

class FeedbackController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        //Set Locale
        $finalLocale = self::localeSetter($request);
        //
        $feedback = Feedback::orderBy('subject', 'asc')->get();

        ##Get Locales
        $localeDropDown = self::localeDropdown($finalLocale);


        return view('feedback.index')
            ->with('feedback', $feedback)
            ->with('localeDropDown', $localeDropDown);


    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Request $request)
    {

        //Set Locale
        $finalLocale = self::localeSetter($request);

        $localeDropDown = self::localeDropdown($finalLocale);

        //Set Country Dropdown
        $countryCollect = self::countryDropdown($finalLocale);
        //Set Language Dropdown
        $languageCollect = self::languageDropdown($finalLocale);
        //Set Locale Language based on header information

        //Set Currency Placeholder
        $currencyPlaceholder = (self::currencyFormatter($finalLocale));

        ##Get Currencies
        $currencies = Currency::orderBy('currency', 'asc')->get();
        ##Get Locales
        $locales = \App\Locale::all();

        return view('feedback.create')
            ->with('currencies', $currencies)
            ->with('finalLocale', $finalLocale)
            ->with('currencyPlaceholder', $currencyPlaceholder)
            ->with('countryCollect', $countryCollect)
            ->with('locales', $locales)
            ->with('localeDropDown', $localeDropDown)
            ->with('languageCollect', $languageCollect);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'company' => 'required|string',
            'name' => 'required|string',
            'email' => 'required|string',
            'subject' => 'required|string',
            'country_id' => 'required|numeric',
            'language_id' => 'required|numeric',
            'currency_id' => 'required|numeric',
            //'date' => 'required|date',
            'amount' => 'required|string',
            'feedback' => 'required|string',

        ]);

        // $date = date('Y-m-d',strtotime($request->date));

        ##Create a user instance
        $feedback = Feedback::create([
            'company' => $request->company,
            'name' => $request->name,
            'email' => $request->email,
            'subject' => $request->subject,
            'country_id' => $request->country_id,
            'language_id' => $request->language_id,
            'currency_id' => $request->currency_id,
            //  'date' => $date,
            'amount' => $request->amount,
            'feedback' => $request->feedback,

        ]);
        ## Save the Feedback
        $feedback->save();

        return redirect('/feedback');
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Request $request, $id)
    {
        //
        $finalLocale = self::localeSetter($request);
        $countryCollect = self::countryDropdown($finalLocale);
        $languageCollect = self::languageDropdown($finalLocale);


        $feedback = Feedback::with('country', 'language', 'currency')->find($id);

        $currencyPlaceholder = (self::currencyFormatter($finalLocale));

        ##Get Currencies
        $currencies = Currency::orderBy('currency', 'asc')->get();

        ##Get Locales
        $localeDropDown = self::localeDropdown($finalLocale);

        return view('feedback.edit')
            ->with('currencies', $currencies)
            ->with('finalLocale', $finalLocale)
            ->with('currencyPlaceholder', $currencyPlaceholder)
            ->with('countryCollect', $countryCollect)
            ->with('feedback', $feedback)
            ->with('localeDropDown', $localeDropDown)
            ->with('languageCollect', $languageCollect);

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
        $this->validate($request, [
            'company' => 'required|string',
            'name' => 'required|string',
            'email' => 'required|string',
            'subject' => 'required|string',
            'country_id' => 'required|numeric',
            'language_id' => 'required|numeric',
            'currency_id' => 'required|numeric',

            'amount' => 'required|string',
            'feedback' => 'required|string',

        ]);

        $feedback = \App\Feedback::find($id);

        ##Create a user instance
        $feedback->company = $request->company;
        $feedback->name = $request->name;
        $feedback->email = $request->email;
        $feedback->subject = $request->subject;
        $feedback->country_id = $request->country_id;
        $feedback->language_id = $request->language_id;
        $feedback->currency_id = $request->currency_id;
        $feedback->amount = $request->amount;
        $feedback->feedback = $request->feedback;


        ## Save the Feedback
        $feedback->save();

        return redirect('/feedback');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
        Feedback::destroy($id);

    }

    /*
     *
     *
     * Returns Array [Final Local, languageCode, countryCode, localHTTP]
     *
     *
     */

    public static function localeSetter(Request $request)
    {


        $getlocale = $request->getlocale;
        $getfulllocale = $request->getfulllocale;
        $getcountry = $request->getcountry;


        /*****************IP ADDRESS INFO ***************************************************/
        //Call to get web user's IP address.  Used to get location based on IP (1. API Call for IP for location)
        $ip = \Request::ip();

        //This is only included for testing.  When testing local IP is ::1 (Loopback). No Need for production
        if ($ip == "::1") {
            //This is for loopback adapter
            $ip = "108.53.16.246";
        }

        //Get Location details on IP address from IPINFO.IO
        $details = json_decode(file_get_contents("http://ipinfo.io/{$ip}/json"));

        //Save Region and Country
        $region = $details->region;
        $country = $details->country;


        //Set supported Countries
        $supportedCountries = ['US', 'CA', 'JP', 'MX', 'DE', 'FR', 'JA'];

        //Set supported languages by API
        $supportedLanguages = ['en', 'de', 'fr', 'ja', 'es'];


        /*
         *
         * HTTP ACCEPT LANGUAGE SETTING
         *
         */
        //Get Primary Accept language from HTTP Header (2. API call for HTTP_ACCEPT_Language)
        $localeHTTP = \Locale::acceptFromHTTP(\Request::server('HTTP_ACCEPT_LANGUAGE'));

        //Get country code from Header Locale (e.g. en_US would result in $countryCode[0]=en & $countryCode[1]=US
        $explodedLocale = explode("_", $localeHTTP);

        //Check to see that there is more than 1 value.
        if (count($explodedLocale) > 1) {
            $languageCode = strtolower($explodedLocale[0]);
            $countryCode = $explodedLocale[1];
        } else {
            //Some countries use both the language and country as the same identifier (e.g. Germany with de)
            $languageCode = $localeHTTP;
            $countryCode = strtoupper($localeHTTP);
            //Japan users JP and is not as the same as the langauge
            if ($languageCode == "ja") {
                $countryCode = "JP";
            }
        }

        //Set Default Language to English in the even that Locale setting is not a supported value
        if (!in_array($languageCode, $supportedLanguages)) {
            $languageCode = 'en';
        }
        //Set Default Language to English in the even that Locale setting is not a supported value
        if (!in_array($countryCode, $supportedCountries)) {
            $countryCode = 'US';
        }


        //Check if running on Windows vs Linux
        if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
            //Change underscores with dashes on Windows so setlocale works correctly
            setlocale(LC_ALL, $localeHTTP);
        } else {
            //Linux uses underscores no need to change string and suffixes .utf8
            setlocale(LC_ALL, $localeHTTP . ".utf8");
        }

        if (is_null($getlocale)) {
            \App::setLocale($languageCode);
        } else {
            $languageCode = $getlocale;
            $countryCode = $getcountry;
            \Cookie::queue(\Cookie::make('language', $getlocale, 90000));
            \Cookie::queue(\Cookie::make('country', $getcountry, 90000));
            \Cookie::queue(\Cookie::make('locale', $getlocale . "_" . $getcountry, 90000));
            \App::setLocale($getlocale);
        }

        $languageCookie = $request->cookie('language');
        $countryCookie = $request->cookie('country');

        if (($languageCookie != null) && ($getlocale == null)) {
            $languageCode = $languageCookie;
            $countryCode = $countryCookie;
            \App::setLocale($languageCode);
        }

        return array($languageCode . "_" . $countryCode, $languageCode, $countryCode, $localeHTTP, $region, $country);
    }

    public static function countryDropdown($finalLocale)
    {
        //Set Dropdowns in Form
        ##Get Countries
        $countries = Country::orderBy('country', 'asc')->get();
        $countryCollect = collect();

        foreach ($countries as $country) {
            foreach ($country->countryNative as $key => $value) {
                if ($key == $finalLocale[0]) {
                    $countryCollect->push(['country_id' => $country->id, 'country' => $value, 'locale' => $country->country_code]);
                }
            }
        }
        // Sorting Dropdown by Sort_Locale_String
        return $countryCollect->sortBy('country', SORT_LOCALE_STRING)->all();
    }

    public static function languageDropdown($finalLocale)
    {
        //Set Dropdowns in Form
        ##Get Countries
        $languages = Language::orderBy('language', 'asc')->get();
        $languageCollect = collect();

        foreach ($languages as $language) {
            foreach ($language->languageNative as $key => $value) {
                if ($key == $finalLocale[0]) {
                    $languageCollect->push(['language_id' => $language->id, 'language' => $value, 'locale' => $language->language_code]);
                }
            }
        }
        // Sorting Dropdown by Sort_Locale_String
        return $languageCollect->sortBy('language', SORT_LOCALE_STRING)->all();

    }

    public static function localeDropdown($finalLocale)
    {
        //Set Dropdowns in Form
        ##Get Countries
        $locales = \App\Locale::orderBy('locale', 'asc')->get();
        $localeCollect = collect();

        foreach ($locales as $locale) {
            foreach ($locale->localeNative as $key => $value) {
                if ($key == $finalLocale[0]) {
                    $localeCollect->push(['language' => $locale->language, 'locale' => $locale->locale, 'full_text_locale' => $value, 'country' => $locale->country]);
                }
            }
        }
        // Sorting Dropdown by Sort_Locale_String
        return $localeCollect->sortBy('full_text_locale', SORT_LOCALE_STRING)->all();

    }


    public static function currencyFormatter($finalLocale)
    {
        //format currency based on Locale
        $formatCurrency = new NumberFormatter($finalLocale[0], NumberFormatter::CURRENCY);
        return $formatCurrency->format(1234);
    }
}
