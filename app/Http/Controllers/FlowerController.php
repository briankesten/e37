<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

class FlowerController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $occasionId = 0;
        //Validation API Arrays
        $supportedMarkets = [ 'us', 'ca', 'uk', 'ru', 'jp', 'de', 'hk'];
        //Set supported languages by API
        $supportedLanguages = ['en','de','fr','ru'];

        //Get Primary Accept language from HTTP Header
        $localeHTTP = \Locale::acceptFromHTTP( \Request::server('HTTP_ACCEPT_LANGUAGE'));
        //Get country code from Header Locale (e.g. en_US would result in $countryCode[0]=en & $countryCode[1]=US
        $explodedLocale = explode("_", $localeHTTP);
        //Set supported markets by API (Used for billingRegion and marketid

        //Check to see that there is more than 1 value.
        if(count($explodedLocale) > 1){
            $languageCode = strtolower($explodedLocale[0]);
            $countryCode =  strtolower($explodedLocale[1]);
            //Through testing I found that UK is GB in the Chrome browser
            if($countryCode == 'gb'){
                $countryCode = 'uk';
            }
        }else{
            //Some countries use both the language and country as the same identifier (e.g. Germany with de)
            $languageCode = $localeHTTP;
            $countryCode = $localeHTTP;
        }

            \App::setLocale($languageCode);

        $colorSamples = array(
            "Red" => "https://placehold.it/15/FF0000/000000?text=+",
            "White" => "https://placehold.it/15/FFFFFF/000000?text=+",
            "Pink" => "https://placehold.it/15/FFC0CB/000000?text=+",
            "Golden" => "https://placehold.it/15/FFFF00/000000?text=+",
            "Purple" => "https://placehold.it/15/800080/000000?text=+",
        );

        //Set Default Language to English in the even that Locale setting is not a supported value
        if(!in_array($languageCode, $supportedLanguages)){
            $languageCode = 'en';
        }
        //Set Default Language to English in the even that Locale setting is not a supported value
        if(!in_array($countryCode, $supportedMarkets)){
            $countryCode = 'us';
        }
       // \Cookie::queue('languageCode', $languageCode, 6000);
       // \Cookie::queue('countryCode', $countryCode, 6000);

        //Updated previous calls to use HTTP Locale Information (Language and Country)
        //Web Service Calls
        //Call 1:
        //Returns the list of languages the flower service supports, that is, the list of languages for which we have localized flower, color and occasion names.
        //http://flowerservice20160406025332.azurewebsites.net/languages?languages=en,de,fr,ru
        $client = new \GuzzleHttp\Client();
        $result = $client->request('GET', 'http://flowerservice20160406025332.azurewebsites.net/languages?languages=' . $languageCode);
        $json_results = $result->getBody();
        //Array Result
        $flowerLanguages = (json_decode($json_results));
        //dd($flowerLanguages);

        //Call 2:
        //http://flowerservice20160406025332.azurewebsites.net/markets?languages=en,de,fr,ru
        //Returns the list of markets the flower service supports.
        $client2= new \GuzzleHttp\Client();
        $result2 = $client2->request('GET', 'http://flowerservice20160406025332.azurewebsites.net/markets?languages=' . $languageCode);
        $json_results2 = $result2->getBody();
        //Array Result
        $flowerMarkets = (json_decode($json_results2));
        //dd($flowerMarkets);

        //Call 3:
        //http://flowerservice20160406025332.azurewebsites.net/flowers?languages=en,de,fr,ru&billingRegion=us
        //Returns the list of flowers available across all markets, along with information about each flower, such as color, description, an image, and price
        $client3= new \GuzzleHttp\Client();
        $result3 = $client3->request('GET', 'http://flowerservice20160406025332.azurewebsites.net/flowers?languages=' . $languageCode ."&billingRegion=" . $countryCode);
        $json_results3 = $result3->getBody();
        //Array Result
        $flowerFlowers = (json_decode($json_results3));
        //dd($flowerFlowers);

        //Call 4:
        //http://flowerservice20160406025332.azurewebsites.net/<marketid>/flowers?languages=en,de,fr,ru&billingRegion=us
        //Returns the list of flowers available in the market that corresponds to the marketid parameter.
        $client4= new \GuzzleHttp\Client();
        $result4 = $client4->request('GET', 'http://flowerservice20160406025332.azurewebsites.net/' . $countryCode . '/flowers?languages=' . $languageCode ."&billingRegion=" . $countryCode);
        $json_results4 = $result4->getBody();
        //Array Result
        $flowerResults = (json_decode($json_results4));
        //dd($flowerFlowers);


        //Call 5:
        //	http://flowerservice20160406025332.azurewebsites.net/<marketid>/occasions?languages=en,de,fr,ru
        //Returns the list of occasions relevant to the market that corresponds to the marketid parameter.
        $client5= new \GuzzleHttp\Client();
        $result5 = $client5->request('GET', 'http://flowerservice20160406025332.azurewebsites.net/' . $countryCode . '/occasions?languages=' . $languageCode);
        $json_results5 = $result5->getBody();
        //Array Result
        $flowerOccasions = (json_decode($json_results5));

        //Get Source File to Display in Template
       // $sourceFile = File::get('assignment4.php');

        //Return View with Information
        return view('flowers')
            ->with('flowerLanguages', $flowerLanguages)
            ->with('flowerMarkets', $flowerMarkets)
            ->with('flowerFlowers', $flowerFlowers)
            ->with('flowerResults', $flowerResults)
            ->with('flowerOccasions', $flowerOccasions)
            ->with('colorSamples', $colorSamples)
            ->with('occasionId', $occasionId)
            ->with('countryCode', $countryCode)
            ->with('languageCode', $languageCode);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $this->validate($request, [
            'languageCode' => 'required',
            'countryCode' => 'required',
            'occasionId' => 'required',
        ]);

        $languageCode = $request->languageCode;
        $countryCode = $request->countryCode;
        $occasionId = $request->occasionId;

        $colorSamples = array(
            "Red" => "https://placehold.it/15/FF0000/000000?text=+",
            "White" => "https://placehold.it/15/FFFFFF/000000?text=+",
            "Pink" => "https://placehold.it/15/FFC0CB/000000?text=+",
            "Golden" => "https://placehold.it/15/FFFF00/000000?text=+",
            "Purple" => "https://placehold.it/15/800080/000000?text=+",
        );

        \App::setLocale($languageCode);

        //Updated previous calls to use HTTP Locale Information (Language and Country)
        //Web Service Calls
        //Call 1:
        //Returns the list of languages the flower service supports, that is, the list of languages for which we have localized flower, color and occasion names.
        //http://flowerservice20160406025332.azurewebsites.net/languages?languages=en,de,fr,ru
        $client = new \GuzzleHttp\Client();
        $result = $client->request('GET', 'http://flowerservice20160406025332.azurewebsites.net/languages?languages=' . $languageCode);
        $json_results = $result->getBody();
        //Array Result
        $flowerLanguages = (json_decode($json_results));

        //Call 2:
        //http://flowerservice20160406025332.azurewebsites.net/markets?languages=en,de,fr,ru
        //Returns the list of markets the flower service supports.
        $client2= new \GuzzleHttp\Client();
        $result2 = $client2->request('GET', 'http://flowerservice20160406025332.azurewebsites.net/markets?languages=' . $languageCode);
        $json_results2 = $result2->getBody();
        //Array Result
        $flowerMarkets = (json_decode($json_results2));

        //Call 5:
        //	http://flowerservice20160406025332.azurewebsites.net/<marketid>/occasions?languages=en,de,fr,ru
        //Returns the list of occasions relevant to the market that corresponds to the marketid parameter.
        $client5= new \GuzzleHttp\Client();
        $result5 = $client5->request('GET', 'http://flowerservice20160406025332.azurewebsites.net/' . $countryCode . '/occasions?languages=' . $languageCode);
        $json_results5 = $result5->getBody();
        //Array Result
        $flowerOccasions = (json_decode($json_results5));


        if($request->occasionId == 0){
            //Call 4:
            //http://flowerservice20160406025332.azurewebsites.net/<marketid>/flowers?languages=en,de,fr,ru&billingRegion=us
            //Returns the list of flowers available in the market that corresponds to the marketid parameter.
            $client4= new \GuzzleHttp\Client();
            $result4 = $client4->request('GET', 'http://flowerservice20160406025332.azurewebsites.net/' . $countryCode . '/flowers?languages=' . $languageCode ."&billingRegion=" . $countryCode);
            $json_results4 = $result4->getBody();
            $flowerResults = (json_decode($json_results4));
        }else{
            //Call 6:
            //http://flowerservice20160406025332.azurewebsites.net/<marketid>/<occasionid>/flowers?languages=en,de,fr,ru&billingRegion=us
            //Returns the list of flowers available in the market that corresponds to the marketid parameter and the occasionid parameter, along with information about each flower such as color, description, an image, and price.
            $url = 'http://flowerservice20160406025332.azurewebsites.net/' . $countryCode . '/'. $occasionId . '/flowers?languages=' . $languageCode . "&billingRegion=" . $countryCode;
            $client6= new \GuzzleHttp\Client();
            $result6 = $client6->request('GET', $url);
            $json_results6 = $result6->getBody();
            //Array Result
            $flowerResults = (json_decode($json_results6));
        }

        return view('flowers')
            ->with('flowerLanguages', $flowerLanguages)
            ->with('flowerMarkets', $flowerMarkets)
            ->with('flowerOccasions', $flowerOccasions)
            ->with('flowerResults', $flowerResults)
            ->with('countryCode', $countryCode)
            ->with('languageCode', $languageCode)
            ->with('colorSamples', $colorSamples)
            ->with('occasionId', $occasionId);


   }

   /**
    * Display the specified resource.
    *
    * @param  int  $id
    * @return \Illuminate\Http\Response
    */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
