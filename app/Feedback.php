<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{
    //
    protected $fillable = [
        'name', 'email', 'company', 'feedback', 'subject', 'date', 'amount', 'language_id', 'country_id', 'currency_id'
    ];

    /*
     *
     *  Schema::create('feedback', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            // User Info
            $table->string('name');
            $table->string('email');
            $table->string('company');
            //Feedback
            $table->text('feedback');
            $table->text('subject');
            $table->date('date');
            $table->integer('amount');


            //Foreign Keys
            $table->integer('language_id')->unsigned();
            $table->integer('country_id')->unsigned();
            $table->integer('currency_id')->unsigned();

            # Foreign Keys - Don't Delete records if user is deleted
            $table->foreign('language_id')->references('id')->on('languages');
            $table->foreign('country_id')->references('id')->on('countries');
            $table->foreign('currency_id')->references('id')->on('currency');
        });
     *
     *
     */

    public function country()
    {
        return $this->belongsTo('App\Country');
    }

    public function language()
    {
        return $this->belongsTo('App\Language');
    }

    public function currency()
    {
        return $this->belongsTo('App\Currency');
    }


}
