<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Currency extends Model
{
    //
    protected $fillable = [
        'currency', 'currency_symbol', 'currency_abbrev'
    ];

    protected $casts = [
        // Will convarted to (Bool)
        'http_accept' => 'array', // Will convarted to (Array)
        'country' => 'array', // Will convarted to (Array)
    ];



    public function feedback()
    {
        return $this->hasMany('App\Feedback');
    }
}
