<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country extends Model
{

    protected $fillable = [
        'country', 'country_code', 'http_accept'
    ];

    protected $casts = [
         // Will convarted to (Bool)
        'countryNative' => 'array', // Will convarted to (Array)
        'http_accept' => 'array',
    ];


    public function feedback()
    {
        return $this->hasMany('App\Feedback');
    }
}
