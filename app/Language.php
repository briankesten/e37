<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Language extends Model
{
    //

    protected $fillable = [
        'language', 'language_code', 'http_accept',
    ];

    protected $casts = [
        // Will convarted to (Bool)
        'languageNative' => 'array', // Will convarted to (Array)
        'http_accept' => 'array', // Will convarted to (Array)

    ];


    public function feedback()
    {
        return $this->hasMany('App\Feedback');
    }
}
