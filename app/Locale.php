<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Locale extends Model
{
    //
    protected $casts = [
        // Will convarted to (Bool)
        'localeNative' => 'array', // Will convarted to (Array)

    ];
}
