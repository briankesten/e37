<?php

use GuzzleHttp\Exception\GuzzleException;
use GuzzleHttp\Client;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function (){
    return view('welcome');
});

//Assignment 1 Route
Route::get('assignment1/{locale?}', function ($locale=null) {
    if(isset($locale)) {
        App::setLocale($locale);
    }

    //************************************************************//
    //*******************Locale Information***********************//
    //************************************************************//

    //Get Locale From HTTP Header
    $lang = Request::server('HTTP_ACCEPT_LANGUAGE');

    //IP Address
    $ipAddress = Request::ip();

    //Get abbreviated Locale using the HTTP Header and the acceptFromHTTP Method
    $localeHTTP = Locale::acceptFromHTTP( Request::server('HTTP_ACCEPT_LANGUAGE'));

    //Code to figure out whether user is using a twelve hour clock
    //I couldn't find a method that checks whether the clock is 12 or 24 so made this function
    $twelveHourCountryCodes = ['US', 'GB', 'PH', 'CA', 'AU', 'NZ', 'IN', 'EG', 'SA', 'CO', 'PK', 'MY'];

    //Get country code from Header Locale (e.g. en_US would result in $countryCode[0]=en & $countryCode[1]=US
    //Get Primary Accept language from HTTP Header
    $localeHTTP = \Locale::acceptFromHTTP( \Request::server('HTTP_ACCEPT_LANGUAGE'));
    //Get country code from Header Locale (e.g. en_US would result in $countryCode[0]=en & $countryCode[1]=US
    $explodedLocale = explode("_", $localeHTTP);
    //Set supported markets by API (Used for billingRegion and marketid

    //Check to see that there is more than 1 value.
    if(count($explodedLocale) > 1){
        $languageCode = strtolower($explodedLocale[0]);
        $countryCode =  strtolower($explodedLocale[1]);
        //Through testing I found that UK is GB in the Chrome browser
        if($countryCode == 'gb'){
            $countryCode = 'uk';
        }
    }else{
        //Some countries use both the language and country as the same identifier (e.g. Germany with de)
        $languageCode = $localeHTTP;
        $countryCode = $localeHTTP;
        //Create new locale that is formatted correctly for functions/methods
        $localeHTTP = $languageCode . "_" . strtoupper($countryCode);
    }

    if(in_array($countryCode[1], $twelveHourCountryCodes))
    {
        $clockFormat = "12 hour clock";
    }
    else
    {
        $clockFormat = "24 hour clock";
    }

    //Default Locale
    $defaultLocale = Locale::getDefault ( );

    //Display Language
    $displayLanguage = Locale::getDisplayLanguage($localeHTTP);

    //Display Locale Region
    $displayRegion = Locale::getDisplayRegion($localeHTTP);

    //Display Locale Script
    $displayScript = Locale::getDisplayScript( 'sl-Latn-IT-nedis', $languageCode);


    //**************************************************************//
    //********************Calendar/Date/Time************************//
    //**************************************************************//

    //Create and International Calendar Instance Using Locale
    $intCalendar = IntlCalendar::createInstance(NULL, $localeHTTP);

    //Get the First Day of the Week for Locale - Returns Integer Value
    $firstDayWeek = $intCalendar->getFirstDayOfWeek ();
    //Find cooresponding name / day abbreviation of first day of week
    $dayName = date('D', strtotime("Saturday +{$firstDayWeek} days"));
    //Returns Calendar Type Based on Locale
    $calendarType = $intCalendar->getType();

    //******************************************************//
    //**************Monetary Section ***********************//
    //******************************************************//

    //Check if running on Windows vs Linux
    if (strtoupper(substr(PHP_OS, 0, 3)) === 'WIN') {
        //Change underscores with dashes on Windows so setlocale works correctly
        setlocale(3, str_replace('_','-',$localeHTTP));
    } else{
        //Linux uses underscores no need to change string and suffixes .utf8
        setlocale(LC_MONETARY, $localeHTTP . ".utf8");
    }

    //Get Monetary Information Based on Locale - Returns Array
    $monetaryArray = localeconv();

    //International Currency Text Symbol  e.g. USD
    $internationalCurrencySymbol = $monetaryArray['int_curr_symbol'];
    //Currency Symbol

    $formatCurrency = new NumberFormatter($localeHTTP, NumberFormatter::CURRENCY);
    $currencyExample =  $formatCurrency->format(1234567);

    $sourceFile = File::get('assignment1.php');
    $resourceFile = File::get('language.php');


    return view('assignment1')
        ->with('lang',$lang)
        ->with('ipAddress', $ipAddress)
        ->with('sourceFile', $sourceFile)
        ->with('localeHTTP', $localeHTTP)
        ->with('clockFormat', $clockFormat)
        ->with('defaultLocale', $defaultLocale)
        ->with('displayLanguage',$displayLanguage)
        ->with('displayRegion', $displayRegion)
        ->with('displayScript', $displayScript)
        ->with('dayName',$dayName)
        ->with('calendarType', $calendarType)
        ->with('internationalCurrencySymbol', $internationalCurrencySymbol)
        ->with('currencyExample',$currencyExample)
        ->with("resourceFile",$resourceFile)
        ;
});

//Assignment 2 Route
Route::get('/assignment2/{language?}/{region?}', function ($language="en", $region="us") {

    //Web Service Calls

    //Call 1:
    //Returns the list of languages the flower service supports, that is, the list of languages for which we have localized flower, color and occasion names.
    //http://flowerservice20160406025332.azurewebsites.net/languages?languages=en,de,fr,ru
    $client = new GuzzleHttp\Client();
    $result = $client->request('GET', 'http://flowerservice20160406025332.azurewebsites.net/languages?languages=' . $language);
    $json_results = $result->getBody();
    //Array Result
    $flowerLanguages = (json_decode($json_results));
    //dd($flowerLanguages);

    //Call 2:
    //http://flowerservice20160406025332.azurewebsites.net/markets?languages=en,de,fr,ru
    //Returns the list of markets the flower service supports.
    $client2= new GuzzleHttp\Client();
    $result2 = $client2->request('GET', 'http://flowerservice20160406025332.azurewebsites.net/markets?languages=' . $language);
    $json_results2 = $result2->getBody();
    //Array Result
    $flowerMarkets = (json_decode($json_results2));
    //dd($flowerMarkets);

    //Call 3:
    //http://flowerservice20160406025332.azurewebsites.net/flowers?languages=en,de,fr,ru&billingRegion=us
    //Returns the list of flowers available across all markets, along with information about each flower, such as color, description, an image, and price
    $client3= new GuzzleHttp\Client();
    $result3 = $client3->request('GET', 'http://flowerservice20160406025332.azurewebsites.net/flowers?languages=' . $language ."&billingRegion=" . $region);
    $json_results3 = $result3->getBody();
    //Array Result
    $flowerFlowers = (json_decode($json_results3));
   //dd($flowerFlowers);

    //Get Source File to Display in Template
    $sourceFile = File::get('assignment2.php');

    //Return View with Information
    return view('assignment2')
        ->with('flowerLanguages', $flowerLanguages)
        ->with('flowerMarkets', $flowerMarkets)
        ->with('flowerFlowers', $flowerFlowers)
        ->with("sourceFile",$sourceFile);

});

//Assignment 3 Route
Route::get('/assignment3/{occasionid?}', function ($occasionid=null) {

    //From Assignment 1: I am taking the Primary User Locale and getting the language and country
    //Get abbreviated Locale using the HTTP Header and the acceptFromHTTP Method
    $localeHTTP = Locale::acceptFromHTTP( Request::server('HTTP_ACCEPT_LANGUAGE'));
    //Get country code from Header Locale (e.g. en_US would result in $countryCode[0]=en & $countryCode[1]=US
    $explodedLocale = explode("_", $localeHTTP);


    if(count($explodedLocale) > 1){
        $languageCode = strtolower($explodedLocale[0]);
        $countryCode =  strtolower($explodedLocale[1]);
        if($countryCode == 'gb'){
            $countryCode = 'uk';
        }
    }else{
        $languageCode = $localeHTTP;
        $countryCode = $localeHTTP;

    }


    //dd($countryCode);

    //Updated previous calls to use HTTP Locale Information (Language and Country)
    //Web Service Calls
    //Call 1:
    //Returns the list of languages the flower service supports, that is, the list of languages for which we have localized flower, color and occasion names.
    //http://flowerservice20160406025332.azurewebsites.net/languages?languages=en,de,fr,ru
    $client = new GuzzleHttp\Client();
    $result = $client->request('GET', 'http://flowerservice20160406025332.azurewebsites.net/languages?languages=' . $languageCode);
    $json_results = $result->getBody();
    //Array Result
    $flowerLanguages = (json_decode($json_results));
    //dd($flowerLanguages);

    //Call 2:
    //http://flowerservice20160406025332.azurewebsites.net/markets?languages=en,de,fr,ru
    //Returns the list of markets the flower service supports.
    $client2= new GuzzleHttp\Client();
    $result2 = $client2->request('GET', 'http://flowerservice20160406025332.azurewebsites.net/markets?languages=' . $languageCode);
    $json_results2 = $result2->getBody();
    //Array Result
    $flowerMarkets = (json_decode($json_results2));
    //dd($flowerMarkets);

    //Call 3:
    //http://flowerservice20160406025332.azurewebsites.net/flowers?languages=en,de,fr,ru&billingRegion=us
    //Returns the list of flowers available across all markets, along with information about each flower, such as color, description, an image, and price
    $client3= new GuzzleHttp\Client();
    $result3 = $client3->request('GET', 'http://flowerservice20160406025332.azurewebsites.net/flowers?languages=' . $languageCode ."&billingRegion=" . $countryCode);
    $json_results3 = $result3->getBody();
    //Array Result
    $flowerFlowers = (json_decode($json_results3));
    //dd($flowerFlowers);

    //Call 4:
    //http://flowerservice20160406025332.azurewebsites.net/<marketid>/flowers?languages=en,de,fr,ru&billingRegion=us
    //Returns the list of flowers available in the market that corresponds to the marketid parameter.
    $client4= new GuzzleHttp\Client();
    $result4 = $client4->request('GET', 'http://flowerservice20160406025332.azurewebsites.net/' . $countryCode . '/flowers?languages=' . $languageCode ."&billingRegion=" . $countryCode);
    $json_results4 = $result4->getBody();
    //Array Result
    $flowerMarketAvailable = (json_decode($json_results4));
    //dd($flowerFlowers);

    //Call 5:
    //	http://flowerservice20160406025332.azurewebsites.net/<marketid>/occasions?languages=en,de,fr,ru
    //Returns the list of flowers available in the market that corresponds to the marketid parameter.
    $client5= new GuzzleHttp\Client();
    $result5 = $client5->request('GET', 'http://flowerservice20160406025332.azurewebsites.net/' . $countryCode . '/occasions?languages=' . $languageCode);
    $json_results5 = $result5->getBody();
    //Array Result
    $flowerOccasions = (json_decode($json_results5));

    //Call 6:
    //http://flowerservice20160406025332.azurewebsites.net/uk/21/flowers?languages=en&billingRegion=uk
    //http://flowerservice20160406025332.azurewebsites.net/<marketid>/<occasionid>/flowers?languages=en,de,fr,ru&billingRegion=us
    //Returns the list of flowers available in the market that corresponds to the marketid parameter and the occasionid parameter, along with information about each flower such as color, description, an image, and price.
    $url = 'http://flowerservice20160406025332.azurewebsites.net/' . $countryCode . '/'. $occasionid . '/flowers?languages=' . $languageCode . "&billingRegion=" . $countryCode;
        $client6= new GuzzleHttp\Client();
        $result6 = $client6->request('GET', $url);
        $json_results6 = $result6->getBody();
        //Array Result
        $flowerOccasionsFlowers = (json_decode($json_results6));


    //Get Source File to Display in Template
    $sourceFile = File::get('assignment3.php');


    //Return View with Information
    return view('assignment3')
        ->with('flowerLanguages', $flowerLanguages)
        ->with('flowerMarkets', $flowerMarkets)
        ->with('flowerFlowers', $flowerFlowers)
        ->with('flowerMarketAvailable', $flowerMarketAvailable)
        ->with('flowerOccasions', $flowerOccasions)
        ->with('flowerOccasionsFlowers', $flowerOccasionsFlowers)
        ->with('countryCode', $countryCode)
        ->with('languageCode', $languageCode)
        ->with("sourceFile",$sourceFile);
});

//Assignment 4 New Route
Route::resource('flowers', 'FlowerController');

//Assignment 4 New Route
Route::resource('feedback', 'FeedbackController');

//Assignment 4 Route
Route::get('/assignment4/{occasionid?}', function ($occasionid=null) {

    //From Assignment 1: I am taking the Primary User Locale and getting the language and country
    //Get abbreviated Locale using the HTTP Header and the acceptFromHTTP Method
    $localeHTTP = Locale::acceptFromHTTP( Request::server('HTTP_ACCEPT_LANGUAGE'));
    //Get country code from Header Locale (e.g. en_US would result in $countryCode[0]=en & $countryCode[1]=US
    $explodedLocale = explode("_", $localeHTTP);

    //Set supported markets by API (Used for billingRegion and marketid
    $supportedMarkets = [ 'us', 'ca', 'uk', 'ru', 'jp', 'de', 'hk'];
    //Set supported languages by API
    $supportedLanguages = ['en','de','fr','ru'];

    if(count($explodedLocale) > 1){
        $languageCode = strtolower($explodedLocale[0]);
        $countryCode =  strtolower($explodedLocale[1]);
        if($countryCode == 'gb'){
            $countryCode = 'uk';
        }
    }else{
        //Some countries use both the language and country as the same identifier (e.g. Germany with de)
        $languageCode = $localeHTTP;
        $countryCode = $localeHTTP;
    }

    //Set Default Language to English in the even that Locale setting is not a supported value
    if(!in_array($languageCode, $supportedLanguages)){
        $languageCode = 'en';
    }
    //Set Default Language to English in the even that Locale setting is not a supported value
    if(!in_array($countryCode, $supportedMarkets)){
        $countryCode = 'us';
    }


      Cookie::queue('languageCode', $languageCode, 6000);
      Cookie::queue('countryCode', $countryCode, 6000);


    //Updated previous calls to use HTTP Locale Information (Language and Country)
    //Web Service Calls
    //Call 1:
    //Returns the list of languages the flower service supports, that is, the list of languages for which we have localized flower, color and occasion names.
    //http://flowerservice20160406025332.azurewebsites.net/languages?languages=en,de,fr,ru
    $client = new GuzzleHttp\Client();
    $result = $client->request('GET', 'http://flowerservice20160406025332.azurewebsites.net/languages?languages=' . $languageCode);
    $json_results = $result->getBody();
    //Array Result
    $flowerLanguages = (json_decode($json_results));
    //dd($flowerLanguages);

    //Call 2:
    //http://flowerservice20160406025332.azurewebsites.net/markets?languages=en,de,fr,ru
    //Returns the list of markets the flower service supports.
    $client2= new GuzzleHttp\Client();
    $result2 = $client2->request('GET', 'http://flowerservice20160406025332.azurewebsites.net/markets?languages=' . $languageCode);
    $json_results2 = $result2->getBody();
    //Array Result
    $flowerMarkets = (json_decode($json_results2));
    //dd($flowerMarkets);

    //Call 3:
    //http://flowerservice20160406025332.azurewebsites.net/flowers?languages=en,de,fr,ru&billingRegion=us
    //Returns the list of flowers available across all markets, along with information about each flower, such as color, description, an image, and price
    $client3= new GuzzleHttp\Client();
    $result3 = $client3->request('GET', 'http://flowerservice20160406025332.azurewebsites.net/flowers?languages=' . $languageCode ."&billingRegion=" . $countryCode);
    $json_results3 = $result3->getBody();
    //Array Result
    $flowerFlowers = (json_decode($json_results3));
    //dd($flowerFlowers);

    //Call 4:
    //http://flowerservice20160406025332.azurewebsites.net/<marketid>/flowers?languages=en,de,fr,ru&billingRegion=us
    //Returns the list of flowers available in the market that corresponds to the marketid parameter.
    $client4= new GuzzleHttp\Client();
    $result4 = $client4->request('GET', 'http://flowerservice20160406025332.azurewebsites.net/' . $countryCode . '/flowers?languages=' . $languageCode ."&billingRegion=" . $countryCode);
    $json_results4 = $result4->getBody();
    //Array Result
    $flowerMarketAvailable = (json_decode($json_results4));
    //dd($flowerFlowers);

    //Call 5:
    //	http://flowerservice20160406025332.azurewebsites.net/<marketid>/occasions?languages=en,de,fr,ru
    //Returns the list of flowers available in the market that corresponds to the marketid parameter.
    $client5= new GuzzleHttp\Client();
    $result5 = $client5->request('GET', 'http://flowerservice20160406025332.azurewebsites.net/' . $countryCode . '/occasions?languages=' . $languageCode);
    $json_results5 = $result5->getBody();
    //Array Result
    $flowerOccasions = (json_decode($json_results5));

    //Call 6:
    //http://flowerservice20160406025332.azurewebsites.net/uk/21/flowers?languages=en&billingRegion=uk
    //http://flowerservice20160406025332.azurewebsites.net/<marketid>/<occasionid>/flowers?languages=en,de,fr,ru&billingRegion=us
    //Returns the list of flowers available in the market that corresponds to the marketid parameter and the occasionid parameter, along with information about each flower such as color, description, an image, and price.
    $url = 'http://flowerservice20160406025332.azurewebsites.net/' . $countryCode . '/'. $occasionid . '/flowers?languages=' . $languageCode . "&billingRegion=" . $countryCode;
    $client6= new GuzzleHttp\Client();
    $result6 = $client6->request('GET', $url);
    $json_results6 = $result6->getBody();
    //Array Result
    $flowerOccasionsFlowers = (json_decode($json_results6));

    //Get Source File to Display in Template
    $sourceFile = File::get('assignment4.php');

    //Return View with Information
    return view('assignment4')
        ->with('flowerLanguages', $flowerLanguages)
        ->with('flowerMarkets', $flowerMarkets)
        ->with('flowerFlowers', $flowerFlowers)
        ->with('flowerMarketAvailable', $flowerMarketAvailable)
        ->with('flowerOccasions', $flowerOccasions)
        ->with('flowerOccasionsFlowers', $flowerOccasionsFlowers)
        ->with('countryCode', $countryCode)
        ->with('languageCode', $languageCode)
        ->with("sourceFile",$sourceFile);

});




